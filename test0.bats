#!/usr/bin/env bats

function host_user {
  host=$1
  user=$2
  lxc exec $host id $user
} 

function  host_superuser {
  host=$1
  user=$2
  lxc exec $host ls /etc/sudoers.d | grep $user
}

function host_not_superuser {
  host=$1
  user=$2
  if host_superuser $host $user
  then
    false
  else
    true
  fi
}

@test "u1 has alice" {
  host_user u1 alice
} 

@test "u1 superuser alice" {
  host_superuser u1 alice
} 

@test "u1 has bob" {
  host_user u1 bob
} 

@test "u1 not superuser bob" {
  host_not_superuser u1 bob
} 

@test "u3 has enid" {
  host_user u3 enid  
}

@test "u3  enid" {
  host_user u3 enid  
}

@test "u3 superuser carol" {
  host_superuser u3 carol
} 

@test "u5 superuser gary" {
  host_superuser u5 gary
} 

@test "u3 not superuser helen" {
  host_not_superuser u3 helen
} 

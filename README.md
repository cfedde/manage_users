# manage_users

manage users and sudoers over a complex of servers

See also:
- https://gitlab.com/sofreeus/sfs-sysops/-/issues/61
- https://gitlab.com/sofreeus/sfs-sysops/-/merge_requests/4

## Assumptions:

- An ansible role
- inventory
- Vars file that define what users go on what hosts
- users are either basic users or god-mode sudoers

## Dev environment for this code

- Five systems
  - u1 to u5
- Two host groups two hosts each
  - orange 
    - u2, u3
  - blue
    - u3, u4, u5
  - Note: u3 is in both blue and orange
  - u1 is in no group (other than all)

- ten users 
  - one basic user on all hosts
  - one god-mode user on all hosts
  - two basic users on orange hosts
  - two basic users on blue hosts
  - two god-mode users on orange hosts
  - two god-mode users on blue hosts

## Test Scenarios

loop over inventory files:

- inventory0.yml -- initial users
- inventory1.yml -- remove bob, add kyle god_mode all hosts
- inventory2.yml -- demote carol
- inventory3.yml -- move fatima from orange to blue, retain god_mode

## Run the Tests

There is a `testit` script that executes the current test scenarios
- delete containers from previous round
- create new containers
- for each test scenario
  - run the play book
  - run the test

## Notes

A convenient `ansible.cfg`:
```
$ cat ansible.cfg
[defaults]

host_key_checking = False
callback_whitelist = timer, profile_tasks
# Human-readable output
stdout_callback = yaml
```
